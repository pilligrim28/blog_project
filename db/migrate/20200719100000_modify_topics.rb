class ModifyTopics < ActiveRecord::Migration[6.0]
    def change
        add_index :topics, :alias, unique: true
        Topic.create :alias => "gruzhiki", :title => "Грузчики"
        Topic.create :alias =>"upakovshiki", :title => "Упаковщики"
        Topic.create :alias => "obvashik", :title => "Обвальщики"
        Topic.create :alias => "raznorabochie", :title => "Разноработчики"
        Topic.create :alias => "moishiki", :title => "Мойщики"
      end
  end
  